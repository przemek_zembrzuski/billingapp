// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const covertFile = require('./modules/convertFileToJson');
const preparedData = require('./modules/preparedData');

const dropzoneInit =()=> {
    const dropzone = document.querySelector('#dropzone');
    dropzone.ondragover = (ev)=>{
        return false;
    }
    dropzone.ondrop=(ev)=>{
        ev.preventDefault();
        covertFile(ev.dataTransfer.files[0].path,preparedData)
    }       
}
(()=>{
    dropzoneInit();
})()
