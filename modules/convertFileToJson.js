const csv = require('csvtojson');

const convertFile = (file,callback) => {
    const jsonArray= [];
    csv({
      delimiter:"auto"
    }).fromFile(file).on('json', (jsonObj) => {
            jsonArray.push(jsonObj);
        })
        .on('done', (error) => {
            callback(jsonArray)
        })
}
module.exports = convertFile;