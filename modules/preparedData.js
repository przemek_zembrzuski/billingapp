const calculateCostNetto = require('./calculateCostNetto');
const totalTime = require('./totalTime');
const howManyCalling = require('./howManyCalling');
const createView = require('./createView');
const createPDF = require('./createPDF');

// create object with data for manipulate
const preparedData = (array)=>{
    const dataObj = {};
    array.map(obj=>{
        dataObj[obj['Station']] = {
            cost:calculateCostNetto(array,obj['Station']),
            totalTime:totalTime(array,obj['Station']),
            howManyCalling:howManyCalling(array,obj['Station'])
        }
    });
    createView(dataObj,createDataForSpecifyLine,createTotalResult,timeInterval(array));
    createPDF(timeInterval(array));

}
//create Header time interval 

const timeInterval = (array)=>{
    const firstDay = array[0]['Date/Time'].slice(0,10);
    const lastDay = array[array.length-1]['Date/Time'].slice(0,10);
    return {firstDay,lastDay}
}
// create sum of lines
const createTotalResult = (dataObj)=>{
    const totalResult = {};

    Object.keys(dataObj).map(objKey=>{
        totalResult['cost'] = (totalResult['cost']||0) + dataObj[objKey]['cost'];
        totalResult['totalTime'] = (totalResult['totalTime']||0) + dataObj[objKey]['totalTime'];
        totalResult['howManyCalling'] = (totalResult['howManyCalling']||0) + dataObj[objKey]['howManyCalling']
    })
    return totalResult
}
//create a sum of specyfie line
const createDataForSpecifyLine = (linesArray,dataObj)=>{
    const resultForLine = {};
    Object.keys(dataObj).map(keys=>{
        if(linesArray.includes(keys)){
            resultForLine['cost'] = (resultForLine['cost']||0) + dataObj[keys]['cost'];
            resultForLine['totalTime'] = (resultForLine['totalTime']||0) + dataObj[keys]['totalTime'];
            resultForLine['howManyCalling'] = (resultForLine['howManyCalling']||0) + dataObj[keys]['howManyCalling']
        }
    })
    return resultForLine
}
module.exports = preparedData;