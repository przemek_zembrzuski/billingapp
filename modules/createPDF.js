const pdf = require('html-pdf');
const path = require('path');
const os = require('os');
const child_process = require('child_process')


const createPDF = (date)=>{
    const filename = `${date.firstDay.split('/').join('-')}TO${date.lastDay.split('/').join('-')}.pdf`;
    const button = document.querySelector('.createPDF');
    const loader = document.querySelector('#loader');
    let filePath  = path.dirname(require.main.filename).split(path.sep);
    filePath.splice(-1,2,'bilingiPDF',filename);
    filePath = filePath.join('/');  
    button.addEventListener('click',()=>{
        loader.style.display = 'block';
        const pdfBody = `
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Biling!!!</title>
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
                        <link rel="stylesheet" href="file://${path.dirname(require.main.filename)}/public/css/pdfStyles.css">
                    </head>
                    <body>
                        <header>
                            <h1>Podsumowania dla organizacji i linii</h1>
                            <p>zakres od ${date.firstDay} do ${date.lastDay}</p>
                        </header>
                        <div class="container">
                            <table class="table table-striped">
                                ${document.querySelector('table').innerHTML}
                            </table>
                        </div>
                    </body>
                </html>
        `     
        const options = { format: 'Letter'};
        pdf.create(pdfBody,options).toFile(filePath,(err,res)=>{
            if(err) return console.log(err)
            //OPEN FOLDER OR FILE
            if(os.platform() === 'win32'){
                //OPEN FOLDER
                child_process.exec(`start "" "${path.dirname(filePath)}"`);
                //OPEN FILE
                // child_process.exec(`start "" "${path.dirname(filePath)}/${filename}"`);
            }else if(os.platform() === 'linux'){
                //OPEN FOLDER
                child_process.exec(`xdg-open "${path.dirname(filePath)}"`);
                //OPEN FILE
                // child_process.exec(`xdg-open "${path.dirname(filePath)}/${filename}"`);
            }
            //reload app
            setTimeout(()=>window.location.reload(true),500);
        })
    })
}

module.exports = createPDF;
