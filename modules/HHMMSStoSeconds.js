module.exports = (data)=>{
    return (parseInt(data['Call Duration'].split(":")[0]) != 0 ? data['Call Duration'].split(":")[0]*3600 : 0)+(parseInt(data['Call Duration'].split(":")[1]) != 0 ? data['Call Duration'].split(":")[1]*60 : 0)+parseInt(data['Call Duration'].split(":")[2]);
}