const addLeadingZero = (time)=>{
  return time < 10 ? `0${time}` : time  
}

const seconds2HHMMSS = (timeInSeconds)=>{
    const hours = timeInSeconds/3600;
    const minutes = (timeInSeconds - Math.floor(hours)*3600)/60;
    const seconds = (timeInSeconds - Math.floor(hours) * 3600 - Math.floor(minutes)*60);
    return `${addLeadingZero(Math.floor(hours))}:${addLeadingZero(Math.floor(minutes))}:${addLeadingZero(Math.floor(seconds))}`
}
module.exports = seconds2HHMMSS;