const HHMMSStoSeconds = require('./HHMMSStoSeconds');
const checkNumber = require('./checkNumber');

const calculateCost = (array,station)=>{
    let total = 0;
    array.map(obj=>{
        if(station === obj['Station']){
            if(checkNumber(obj) === 'mobile'){
                total += ((HHMMSStoSeconds(obj)/86400)*24*60*0.45)
            }else if(checkNumber(obj) === 'landline'){
                total += ((HHMMSStoSeconds(obj)/86400)*24*60*0.07)
            }else{
                total += (total[obj.Station] || 0)
            }
        }
    })
    return total
}
module.exports = calculateCost