const calculateVAT = require('./calculateVAT');
const calculateCostBrutto = require('./calculateCostBrutto');
const seconds2HHMMSS = require('./seconds2HHMMSS');


const createView = (dataObj, createDataForSpecifyLine, createTotalResult,timeInterval) => {
    const table = document.querySelector('table');
    const container = document.querySelector('.container');
    const header = document.querySelector('header');
    const spanFirstDay = document.querySelector('.firstDay');
    const spanLastDay = document.querySelector('.lastDay');
    const timeIntervalData = timeInterval;
    let rows = '';
    Object.keys(dataObj).map(key => {
        rows += `<tr>
                <td>${key}</td>
                <td>${(dataObj[key].cost).toFixed(2)}</td>
                <td>${seconds2HHMMSS(dataObj[key].totalTime)}</td>
                <td>${dataObj[key].howManyCalling}</td>
                <td>23%</td>
                <td>${calculateVAT(dataObj[key].cost,calculateCostBrutto(dataObj[key].cost))}</td>
                <td>${calculateCostBrutto(dataObj[key].cost)}</td>
            </tr>
        `;
    })
    const totalResult = createTotalResult(dataObj)
    let totalRow =
    `<tr class="table-danger">
        <td>Razem</td>
        <td>${(totalResult.cost).toFixed(2)}</td>                    
        <td>${seconds2HHMMSS(totalResult.totalTime)}</td>
        <td>${totalResult.howManyCalling}</td>
        <td>23%</td>                                       
        <td>${calculateVAT(totalResult.cost,calculateCostBrutto(totalResult.cost))}</td>
        <td>${calculateCostBrutto(totalResult.cost)}</td>                   
    </tr>                                   
`;
const administrationData = createDataForSpecifyLine(['234', '239', '430', '431', '432', '434', '435', '436', '437', '438', '439','749'], dataObj);
    let administrationRow =
        `<tr class="table-info">
            <td>Administracja</td>
            <td>${(administrationData.cost).toFixed(2)}</td>                    
            <td>${seconds2HHMMSS(administrationData.totalTime)}</td>
            <td>${administrationData.howManyCalling}</td>
            <td>23%</td>                                       
            <td>${calculateVAT(administrationData.cost,calculateCostBrutto(administrationData.cost))}                    </td>
            <td>${calculateCostBrutto(administrationData.cost)}</td>                   
        </tr>                                   
    `;
    const gymData = createDataForSpecifyLine(['433'], dataObj);
    let gymRow =
        `<tr class="table-info">
        <td>Sala Sportowa</td>
        <td>${(gymData.cost).toFixed(2)}</td>
        <td>${seconds2HHMMSS(gymData.totalTime)}</td>
        <td>${gymData.howManyCalling}</td>
        <td>23%</td>
        <td>${calculateVAT(gymData.cost,calculateCostBrutto(gymData.cost))}</td>
        <td>${calculateCostBrutto(gymData.cost)}</td>
    </tr>
`;
    spanFirstDay.innerText = timeIntervalData.firstDay;
    spanLastDay.innerText = timeIntervalData.lastDay
    rows+=totalRow;
    rows+=administrationRow;
    rows+=gymRow;
    table.children[0].insertAdjacentHTML('beforeend', rows);
    document.body.removeChild(document.querySelector('#dropzone'));
    header.style.display = 'flex';
    container.style.display = 'flex';
}
module.exports = createView;