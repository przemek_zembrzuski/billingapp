const HHMMSStoSeconds = require('./HHMMSStoSeconds');

const totalTime = (array,station)=>{
    let duration = 0;
    array.map(obj=>{
        if(station === obj['Station']) duration += +HHMMSStoSeconds(obj)
    })
    return duration
}
module.exports = totalTime


