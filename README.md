## BilingApp


Billing App is an application to transform billing data into more readable and display calculate cost, sum of calls etc. It's based on Electron.

## Installation

git clone https://przemek_zembrzuski@bitbucket.org/przemek_zembrzuski/billingapp.git billingapp  
cd billingapp  
npm install  
add file *numbers.txt* with the free numbers (one by one)  
npm start

## License

BilingApp is ISC licensed
